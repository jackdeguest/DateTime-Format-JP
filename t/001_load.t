# -*- perl -*-
BEGIN
{
    use strict;
    use warnings;
    use Test::More tests => 2;
    use_ok( 'DateTime::Format::JP' );
};

my $fmt = DateTime::Format::JP->new;
isa_ok( $fmt, 'DateTime::Format::JP' );

__END__

